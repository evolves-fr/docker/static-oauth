package main

import (
	"embed"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/apple"
	"github.com/markbates/goth/providers/github"
	"github.com/markbates/goth/providers/gitlab"
	"github.com/markbates/goth/providers/google"
	"github.com/markbates/goth/providers/microsoftonline"
	"github.com/markbates/goth/providers/slack"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/gommon"
)

const (
	authSessionName = "_auth"
)

var version = "0.0.0-dev"

var (
	//go:embed public/login.gohtml
	loginTemplate []byte

	//go:embed public/*
	public embed.FS
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Category: "Server",
			Name:     "debug",
			Usage:    "Enable debug mode",
			EnvVars:  []string{"ENTRYPOINT_DEBUG"},
			Value:    false,
		},
		&cli.StringFlag{
			Category: "Server",
			Name:     "directory",
			Usage:    "Static directory path",
			EnvVars:  []string{"ENTRYPOINT_DIRECTORY"},
			Value:    "./static",
		},
		&cli.StringFlag{
			Category: "Server",
			Name:     "address",
			Usage:    "Server listen address and port",
			EnvVars:  []string{"ENTRYPOINT_ADDRESS"},
			Value:    ":3000",
		},
		&cli.StringFlag{
			Category: "Server",
			Name:     "url",
			Usage:    "Base url",
			EnvVars:  []string{"ENTRYPOINT_BASEURL"},
			Value:    "http://localhost:3000",
		},
		&cli.StringFlag{
			Category: "Server",
			Name:     "secret",
			Usage:    "Secret",
			EnvVars:  []string{"ENTRYPOINT_SECRET"},
			Required: true,
		},
		&cli.StringSliceFlag{
			Category: "OAuth",
			Name:     "domains",
			Usage:    "Allowed domains",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_DOMAINS"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "apple-key",
			Usage:    "Apple OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_APPLE_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "apple-secret",
			Usage:    "Apple OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_APPLE_SECRET"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "github-key",
			Usage:    "Github OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GITHUB_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "github-secret",
			Usage:    "Github OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GITHUB_SECRET"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "gitlab-key",
			Usage:    "Gitlab OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GITLAB_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "gitlab-secret",
			Usage:    "Gitlab OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GITLAB_SECRET"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "google-key",
			Usage:    "Google OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GOOGLE_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "google-secret",
			Usage:    "Google OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_GOOGLE_SECRET"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "microsoft-key",
			Usage:    "Microsoft OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_MICROSOFT_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "microsoft-secret",
			Usage:    "Microsoft OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_MICROSOFT_SECRET"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "slack-key",
			Usage:    "Slack OAuth credential key",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_SLACK_KEY"},
		},
		&cli.StringFlag{
			Category: "OAuth",
			Name:     "slack-secret",
			Usage:    "Slack OAuth credential secret",
			EnvVars:  []string{"ENTRYPOINT_OAUTH_SLACK_SECRET"},
		},
	}

	app := &cli.App{
		Name:      "entrypoint",
		Usage:     "Secure static web sites with OAuth",
		Copyright: "EVOLVES SAS",
		Authors:   []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Version:   version,
		Flags:     flags,
		Before:    before,
		Action:    action,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func before(ctx *cli.Context) error {
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	return nil
}

func action(ctx *cli.Context) error {
	// Initialize application context
	app := &App{
		Secret:              []byte(ctx.String("secret")),
		Directory:           ctx.String("directory"),
		ServerListenAddress: ctx.String("address"),
		BaseURL:             ctx.String("url"),
		AllowDomains:        ctx.StringSlice("domains"),
	}

	// Initialize cookie session storage
	app.store = sessions.NewCookieStore(app.Secret)
	app.store.MaxAge(86400 * 30)
	app.store.Options.Path = "/"
	app.store.Options.HttpOnly = true
	app.store.Options.Secure = strings.HasPrefix(app.BaseURL, "https")
	gothic.Store = app.store

	// Load OAuth providers
	app.providers = loadProviders(app.BaseURL, []Provider{
		{Name: "apple", Key: ctx.String("apple-key"), Secret: ctx.String("apple-secret")},
		{Name: "github", Key: ctx.String("github-key"), Secret: ctx.String("github-secret")},
		{Name: "gitlab", Key: ctx.String("gitlab-key"), Secret: ctx.String("gitlab-secret")},
		{Name: "google", Key: ctx.String("google-key"), Secret: ctx.String("google-secret")},
		{Name: "microsoft", Key: ctx.String("microsoft-key"), Secret: ctx.String("microsoft-secret")},
		{Name: "slack", Key: ctx.String("slack-key"), Secret: ctx.String("slack-secret")},
	}...)

	// Initialize router
	router := mux.NewRouter()
	router.Use(loggerMiddleware)
	router.Handle("/auth/style.css", http.StripPrefix("/auth", http.HandlerFunc(publicHandle)))
	router.Handle("/auth/favicon.ico", http.StripPrefix("/auth", http.HandlerFunc(publicHandle)))
	router.Handle("/auth/icons.ttf", http.StripPrefix("/auth", http.HandlerFunc(publicHandle)))
	router.Handle("/auth/icons.woff", http.StripPrefix("/auth", http.HandlerFunc(publicHandle)))
	router.HandleFunc("/auth", app.Index)
	router.HandleFunc("/auth/logout", app.Logout)
	router.HandleFunc("/auth/{provider}", app.Login)
	router.HandleFunc("/auth/{provider}/callback", app.Callback)
	router.PathPrefix("/").Handler(app)

	// Start server
	return http.ListenAndServe(ctx.String("address"), router)
}

func publicHandle(w http.ResponseWriter, r *http.Request) {
	file, err := public.ReadFile(filepath.Join("public", r.URL.Path))
	if err != nil {
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", http.DetectContentType(file))
	_, _ = w.Write(file)

	return
}

func loggerMiddleware(next http.Handler) http.Handler {
	return handlers.CombinedLoggingHandler(os.Stdout, next)
}

type App struct {
	Secret              []byte
	Directory           string
	ServerListenAddress string
	BaseURL             string
	AllowDomains        []string
	providers           []string
	store               *sessions.CookieStore
}

func (app *App) Index(w http.ResponseWriter, r *http.Request) {
	provider, email, err := app.getUser(r)

	tpl := template.Must(template.New("login.html").Parse(string(loginTemplate)))

	data := map[string]any{
		"IsLogged":           err == nil,
		"Provider":           provider,
		"Email":              email,
		"ProvidersAvailable": app.providers,
		"ProvidersMap":       providersMap,
	}

	if err := tpl.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (app *App) Login(w http.ResponseWriter, r *http.Request) {
	if app.isLogged(r) {
		http.Redirect(w, r, "/auth", http.StatusTemporaryRedirect)
		return
	} else {
		gothic.BeginAuthHandler(w, r)
	}
}

func (app *App) Callback(w http.ResponseWriter, r *http.Request) {
	user, err := gothic.CompleteUserAuth(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	if len(app.AllowDomains) > 0 {
		if !gommon.Has(strings.Split(user.Email, "@")[1], app.AllowDomains...) {
			http.Error(w, "domain not authorized", http.StatusUnauthorized)
			return
		}
	}

	token, err := app.generateToken(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err = app.session(authSessionName, r, w).Set("jwt", token); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func (app *App) Logout(w http.ResponseWriter, r *http.Request) {
	if app.isLogged(r) {
		_ = app.session(authSessionName, r, w).Clean()
	}

	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (app *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !app.isLogged(r) {
		http.Redirect(w, r, "/auth", http.StatusTemporaryRedirect)
		return
	}

	w.Header().Set("Cache-Control", "no-cache")
	http.FileServer(http.Dir(app.Directory)).ServeHTTP(w, r)
}

func (app *App) generateToken(user goth.User) (string, error) {
	signer := jwt.New(jwt.SigningMethodHS256)

	claims := signer.Claims.(jwt.MapClaims)
	claims["provider"] = user.Provider
	claims["email"] = user.Email

	return signer.SignedString(app.Secret)
}

func (app *App) parseToken(str string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(str, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("there was an error in parsing")
		}
		return app.Secret, nil
	})
	if err != nil {
		return nil, err
	}

	return token.Claims.(jwt.MapClaims), nil
}

func (app *App) getUser(r *http.Request) (string, string, error) {
	token, err := app.session(authSessionName, r, nil).Get("jwt")
	if err != nil {
		return "", "", err
	}

	claims, err := app.parseToken(token)
	if err != nil {
		return "", "", err
	}

	return claims["provider"].(string), claims["email"].(string), nil
}

func (app *App) isLogged(r *http.Request) bool {
	if _, err := app.session(authSessionName, r, nil).Get("jwt"); err == nil {
		return true
	}

	return false
}

func (app *App) session(name string, r *http.Request, w http.ResponseWriter) _session {
	session, _ := app.store.Get(r, name)
	return _session{request: r, response: w, store: session}
}

type _session struct {
	request  *http.Request
	response http.ResponseWriter
	store    *sessions.Session
}

func (s _session) Set(key, value string) error {
	s.store.Values[key] = value
	return s.store.Save(s.request, s.response)
}

func (s _session) Get(key string) (string, error) {
	if s.store.Values[key] == nil {
		return "", fmt.Errorf("could not find a matching session for this request")
	}

	return s.store.Values[key].(string), nil
}

func (s _session) Clean() error {
	s.store.Options.MaxAge = -1
	s.store.Values = make(map[any]any)
	return s.store.Save(s.request, s.response)
}

var providersMap = map[string]string{
	"apple":     "Apple",
	"github":    "Github",
	"gitlab":    "Gitlab",
	"google":    "Google",
	"microsoft": "Microsoft",
	"slack":     "Slack",
}

type Provider struct {
	Name   string
	Key    string
	Secret string
}

func loadProviders(baseURL string, providers ...Provider) []string {
	var available = make([]string, 0)

	for _, provider := range providers {
		if gommon.Empty(provider.Name) || gommon.Empty(provider.Key) || gommon.Empty(provider.Secret) {
			continue
		}

		callbackURL := fmt.Sprintf("%s/auth/%s/callback", baseURL, provider.Name)

		switch provider.Name {
		case "apple":
			goth.UseProviders(apple.New(provider.Key, provider.Secret, callbackURL, http.DefaultClient))
		case "github":
			goth.UseProviders(github.New(provider.Key, provider.Secret, callbackURL))
		case "gitlab":
			goth.UseProviders(gitlab.New(provider.Key, provider.Secret, callbackURL))
		case "google":
			goth.UseProviders(google.New(provider.Key, provider.Secret, callbackURL))
		case "microsoft":
			goth.UseProviders(microsoftonline.New(provider.Key, provider.Secret, callbackURL))
		case "slack":
			goth.UseProviders(slack.New(provider.Key, provider.Secret, callbackURL))
		}

		available = append(available, provider.Name)
	}

	return available
}
