########################################################################################################################
# Builder
########################################################################################################################
FROM golang:1.19-bullseye AS builder
WORKDIR /go/src/gitlab.com/evolves-fr/docker/static-oauth
ARG VERSION
ENV GO111MODULE on
ENV CGO_ENABLED 0
RUN apt update && apt install -y ca-certificates && update-ca-certificates
COPY . .
RUN go mod download && go build -o /bin/entrypoint -ldflags="-X main.Version=${VERSION}" .

########################################################################################################################
# Main
########################################################################################################################
FROM scratch

ENV ENTRYPOINT_DEBUG            "false"
ENV ENTRYPOINT_DIRECTORY        "/var/www/html"
ENV ENTRYPOINT_ADDRESS          ":80"

ENTRYPOINT                      ["/bin/entrypoint"]
WORKDIR                         /var/www/html
EXPOSE                          80

COPY --from=builder /bin/entrypoint /bin/entrypoint
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
